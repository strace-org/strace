FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > strace.log'

COPY strace .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' strace
RUN bash ./docker.sh

RUN rm --force --recursive strace
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD strace
